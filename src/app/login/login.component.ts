import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CrudService } from "../../curd.service";
import { JwtHelperService } from "@auth0/angular-jwt";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  // @Input() userDetails = {username: '', password:''}
  userForm: FormGroup;
  
  constructor(public crud: CrudService, public router: Router, private fb: FormBuilder) { }

  ngOnInit(): void {
    this.userForm = this.fb.group({
      "username": [''],
      "password": ['']
    })
  }
  addUser() {
    this.crud.createUser(this.userForm.value).subscribe((data: {}) => {
      console.log(data);
      // this.router.navigate(['/new']);
    })
  }

  login() {
    this.crud.login(this.userForm.value
      ).subscribe((data) => {
        localStorage.setItem('userToken', JSON.stringify(data));
        let helper = new JwtHelperService();
        let decodedToken = helper.decodeToken(JSON.stringify(data));
        console.log(decodedToken.user);
        console.log(data);
        // if(data){
        //   alert('Login Sucessful');
        //   this.router.navigateByUrl('home');
        // }else {
        //   this.router.navigateByUrl('error')
        // }
      })
  }
}
