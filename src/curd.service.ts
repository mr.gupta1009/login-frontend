import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from './user';
@Injectable({
    providedIn: 'root'
  })
export class CrudService {
     apiURL = 'http://localhost:3000';
    handleError: any;
    constructor(private http: HttpClient) { }

httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      }


      createUser(user): Observable<User> {
        return this.http.post<User>(`${this.apiURL}/user/new`, JSON.stringify(user), this.httpOptions);
      }

      login(user): Observable<User> {
        return this.http.post<User>(`${this.apiURL}/user/login`, JSON.stringify(user), this.httpOptions);
      }
  
    }
   
    

      



